from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
from django.db import models
from django.utils import timezone
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin

GENDER_CHOICES = (
    (0, 'male'),
    (1, 'female'),
)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Email required')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be a staff'
            )
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be a superuser'
            )

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, max_length=255, blank=False)
    first_name = models.CharField('first name', max_length=150, blank=True)
    last_name = models.CharField('last name', max_length=150, blank=True)
    idNo = models.CharField('ID NO', unique=True, max_length=150, blank=True)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=0)
    mobile = models.PositiveBigIntegerField('mobile', unique=True, null=True, blank=True)
    is_staff = models.BooleanField('staff status', default=False)
    is_active = models.BooleanField('active', default=False)
    is_superuser = models.BooleanField('superuser', default=False)
    date_joined = models.DateTimeField('date joined', default=timezone.now)

    USERNAME_FIELD = 'email'
    objects = UserManager()

    def __str__(self):
        return self.email

    def full_name(self):
        return self.first_name + " " + self.last_name


class Sacco(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=15)
    regNo = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.regNo


class VehicleType(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name


class RouteType(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name


class Vehicle(models.Model):
    vehicleType = models.ForeignKey(
        VehicleType, on_delete=models.CASCADE)
    noPlate = models.CharField(max_length=100)
    make = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    routeType = models.ForeignKey(
        RouteType, on_delete=models.CASCADE)
    route = models.CharField(max_length=100)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE)
    sacco = models.ForeignKey(
        Sacco, on_delete=models.CASCADE, related_name='owned_vehicles')
    conductor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='conductor_vehicles')
    driver = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='driver_vehicles')
    admin = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='admin_vehicles')

    def __str__(self):
        return self.nickname
