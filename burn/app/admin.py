from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from .models import User, Sacco, RouteType, VehicleType, Vehicle


class user(admin.ModelAdmin):
    list_display = ['email', 'first_name', 'last_name', 'idNo', 'gender', 'mobile', 'date_joined']


admin.site.register(User, user)


class sacco(admin.ModelAdmin):
    list_display = ['email', 'name', 'phone', 'regNo']


admin.site.register(Sacco, sacco)


class vehicle(admin.ModelAdmin):
    list_display = ['name', 'description', 'created']


admin.site.register(VehicleType, vehicle)


class route(admin.ModelAdmin):
    list_display = ['name', 'description', 'created']


admin.site.register(RouteType, route)


class vehicleR(admin.ModelAdmin):
    list_display = ['vehicleType', 'noPlate', 'make', 'brand', 'nickname', 'routeType', 'route', 'owner', 'sacco',
                    'conductor', 'driver', 'admin']


admin.site.register(Vehicle, vehicleR)
